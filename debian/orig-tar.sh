#/bin/sh -e

VERSION=$2
TAR=../jenkins-remoting_$VERSION.orig.tar.xz
DIR=jenkins-remoting-$VERSION
mkdir -p $DIR

# Unpack ready fo re-packing
tar -xf ../$3 -C $DIR --strip-components=1
rm ../$3

# Repack excluding stuff we don't need
XZ_OPT=--best tar -cJvf $TAR \
    --exclude '*.jar' \
    --exclude '*.class' \
    --exclude 'CVS' \
    --exclude '.svn' \
    --exclude 'dummy.keystore' \
    $DIR

rm -rf $DIR
